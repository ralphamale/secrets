module FriendshipsHelper

  def can_friend?(out_friend_id, in_friend_id)
    !Friendship.exists?({out_friend_id: out_friend_id, in_friend_id: in_friend_id}) &&
    in_friend_id != current_user.id
  end

  def can_unfriend(out_friend_id, in_friend_id)
    return Friendship.find_by_out_friend_id_and_in_friend_id(out_friend_id,in_friend_id)


  end
end
