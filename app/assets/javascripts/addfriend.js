$(document).ready(function() {
  $('.friend-buttons').on('submit', 'form', function(event) {
    event.preventDefault();
    var in_friend_id = parseInt($(event.currentTarget).attr("data-infriend"));
    var friendship_id = parseInt($(event.currentTarget).attr("data-friendshipid"))

    var form = this;
    var url, data, buttonVal;

    if ($(form).attr("class") == "friend"){
      url = "/users/" + in_friend_id + "/friendships.json";
      data = {};
      buttonVal = "friending..."

    } else {
      url = "/users/" + in_friend_id + "/friendships/" + friendship_id + ".json";
      data = {"_method":"delete"};
      buttonVal = "unfriending..."
    }



    var req = $.ajax({
      url: url,
      data: data,
      type:'post',
      success: function(resp) {
        var class_name = $(form).attr("class");
        $(form).find(':submit').attr("disabled",false).val(buttonVal).attr("value", class_name);
        $(form).closest("div").find(".unfriend").attr("data-friendshipid", resp.id);
        $(form).closest('div').toggleClass('is-friends');
      },
      errors: function(resp) {
        console.log("Error");
      },
      beforeSend: function() {
        $(form).find(':submit').attr("disabled",true).val(buttonVal);
      }
    })

    // var success = function(resp) {
    //   alert("success");
    //   var class_name = $(form).attr("class");
    //   $(form).find(':submit').attr("disabled",false).val(buttonVal).attr("value", class_name);
    //
    //
    //   $(form).closest("div").find(".unfriend").attr("data-friendshipid", resp.id);
    //
    //   $(form).closest('div').toggleClass('is-friends');
    //   console.log(resp);
    // }
    //
    // var error = function(resp) {
    //   alert("fail");
    //   console.log(resp);
    // }
    //
    // req.then(success, error);

  });


});