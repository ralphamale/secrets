class SecretsController < ApplicationController

  def new

    @secret = Secret.new


    # @secret.recipient_id = params[:user_id]

  end

  def create
    secret = Secret.new(params[:secret])

    secret.author_id = current_user.id

    secret.tag_ids = params[:tag][:tag_ids]

    if secret.save

      respond_to do |format|
        format.html { redirect_to users_url }

        format.json { render :json => secret}
      end

    else
      flash.now[:errors] = secret.errors.full_messages
      render :json => params
      # render :new
    end

  end

end
