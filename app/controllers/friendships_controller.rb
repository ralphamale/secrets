class FriendshipsController < ApplicationController

  def create
    sleep(1)
    @friendship = Friendship.new

    @friendship.out_friend_id = current_user.id
    @friendship.in_friend_id = params[:user_id]

    @friendship.save!

    respond_to do |format|
      format.html { redirect_to users_url }

      format.json { render :json => @friendship}
    end

  end

  def destroy
    @friendship = Friendship.find_by_out_friend_id_and_in_friend_id(current_user.id,params[:user_id]);

    # if
    @friendship.destroy
    respond_to do |format|
      format.html { redirect_to users_url }

      format.json { render :json => @friendship}
    end

    # else
    #   flash[:errors] = @friendship.errors.full_messages
    #   redirect_to :back
    # end
  end

end
